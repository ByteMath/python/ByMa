.. _user:

################
ByMa user guide
################

This guide is an overview and explains the important features;
details are found in :ref:`reference`.

.. toctree::
   :caption: Getting started
   :maxdepth: 1

   whatisbyma
   Installation
   quickstart

