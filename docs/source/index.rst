.. ByMa documentation master file, created by
   sphinx-quickstart on Tue Apr  2 22:57:25 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ByMa's documentation
================================


.. toctree::
   :maxdepth: 5
   :hidden:

   User Guide <user/index>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


**Version**: |version|

   
**Useful links**:
`Source Repository <https://gitlab.com/ByteMath/python/ByMa>`_ |
`Issue Tracker <https://gitlab.com/ByteMath/python/ByMa/-/issues>`_ |


.. grid:: 2

    .. grid-item-card::

        Getting started
        ^^^^^^^^^^^^^^^

        New to ByMa? Check out the Absolute Beginner's Guide. It contains an
        introduction to ByMa's main concepts and links to additional tutorials.

        +++

        .. button-ref:: user/quickstart
            :expand:
            :color: secondary
            :click-parent:

            To the absolute beginner's guide

    .. grid-item-card::

        User guide
        ^^^^^^^^^^

        The user guide provides in-depth information on the
        key concepts of ByMa with useful background information and explanation.

        +++

        .. button-ref:: user/index
            :expand:
            :color: secondary
            :click-parent:

            To the user guide

    .. grid-item-card::

        API reference
        ^^^^^^^^^^^^^

        The reference guide contains a detailed description of the functions,
        modules, and objects included in ByMa. The reference describes how the
        methods work and which parameters can be used. It assumes that you have an
        understanding of the key concepts.

        +++

        .. button-ref:: autoapi/index
            :expand:
            :color: secondary
            :click-parent:

            To the reference guide
