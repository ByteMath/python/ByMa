:orphan:

:py:mod:`byma._version`
=======================

.. py:module:: byma._version


Module Contents
---------------

.. py:data:: TYPE_CHECKING
   :value: False

   

.. py:data:: VERSION_TUPLE

   

.. py:data:: version
   :type: str

   

.. py:data:: version_tuple
   :type: VERSION_TUPLE

   

