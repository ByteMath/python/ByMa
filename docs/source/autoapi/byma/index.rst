byma
====

.. py:module:: byma

.. autoapi-nested-parse::

   ByMa: A scientific computing package for Python
   ================================================



   Subpackages
   -----------
   Using any of these subpackages requires an explicit import. For example,
   ``import byma.nuby``.

   ::

    nuby                         --- Numerical Bifurcation Analysis Tools
    iteral                       --- Tools for iterative algorithms
    pyplot                       --- Tools for plotting functions
    interface                    --- Interface functions
    numy                         --- Basic Numerical Methods functions



Subpackages
-----------

.. toctree::
   :maxdepth: 1

   /autoapi/byma/interface/index
   /autoapi/byma/iteral/index
   /autoapi/byma/nuby/index
   /autoapi/byma/numy/index
   /autoapi/byma/pyplot/index


