byma.numy.Numy
==============

.. py:module:: byma.numy.Numy


Classes
-------

.. autoapisummary::

   byma.numy.Numy.Numy


Module Contents
---------------

.. py:class:: Numy

   Defines default options for the numerical methods sub-package


