byma.nuby.Bifurcation
=====================

.. py:module:: byma.nuby.Bifurcation


Classes
-------

.. autoapisummary::

   byma.nuby.Bifurcation.Bifurcation


Module Contents
---------------

.. py:class:: Bifurcation

   Defines default options for the Bifurcation package


