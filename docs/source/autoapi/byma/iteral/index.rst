byma.iteral
===========

.. py:module:: byma.iteral


Submodules
----------

.. toctree::
   :maxdepth: 1

   /autoapi/byma/iteral/GradientDescent/index
   /autoapi/byma/iteral/Iteral/index
   /autoapi/byma/iteral/NesterovMethod/index
   /autoapi/byma/iteral/Newton/index
   /autoapi/byma/iteral/OrthogonalSubspace/index


Classes
-------

.. autoapisummary::

   byma.iteral.Iteral


Package Contents
----------------

.. py:class:: Iteral

   Defines default options for the Iterative methods sub-package


