byma.iteral.Iteral
==================

.. py:module:: byma.iteral.Iteral


Classes
-------

.. autoapisummary::

   byma.iteral.Iteral.Iteral


Module Contents
---------------

.. py:class:: Iteral

   Defines default options for the Iterative methods sub-package


