
class Iteral:
    
    '''Defines default options for the Iterative methods sub-package'''

    def __init__(self):
        self._opts = {'maxit': 1e4, 'tol': 1e-4, 'method': 'general'}
