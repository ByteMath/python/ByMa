"""
Notes
-----

This module is has basic numerical methods tools
"""

from .integration import *
from .regression import *
from .Numy import Numy

__all__ = ['Numy', 'euler', 'regression']


import warnings