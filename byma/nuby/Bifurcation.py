
class Bifurcation:
    
    '''Defines default options for the Bifurcation package'''

    def __init__(self):
        self._opts = {'maxit': 1e4, 'tol': 1e-4}